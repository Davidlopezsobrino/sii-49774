// MundoServidor.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#include<stdio.h>
#include<stdlib.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/mman.h>
#include<unistd.h>
#include<string.h>
#include<errno.h>
#include<Socket.h>
#include <pthread.h>


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DatosMemCompartida.h"
//#include "Esfera.h"
//#include "Raqueta.h"




class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();

	

	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void RecibeComandosJugador();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	

	int puntos1;
	int puntos2;

	int tuberia;
		
	DatosMemCompartida datos;
	DatosMemCompartida *m;
	int mem;

//Identificador Fifo servidor cliente
	int fifosc;
	char buffersc[200];
//Identificador Fifo cliente-servidor
	int fifocs;
//Identificador del thread
	pthread_t thrd;
//Declaramos dos socket 
	Socket scon;
	Socket scom;
	int socket;
	char ip[1024];
//Comunicacion con varios Clientes
	Socket servidor;
	std::vector<Socket> conexiones;
	void GestionarConexiones();
	int acabar;
	
	std::vector<char *> nombres;
	char nombre[200];
	char nombre_previo[200];
	char espectadores[200][200];
	
	int num_espectadores;
	int mostrar_jugador;
	pthread_t thid_clientes;
	

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
