// MundoServidor.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


  #include <sys/types.h>
  #include <sys/stat.h>
  #include <fcntl.h> 
  #include <unistd.h>
  #include <errno.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void * hilo_comandos(void * d);
void * hilo_gestion_sockets(void * d);

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//close(tuberia);
	close(fifosc);
	pthread_exit(0);
	scon.Close();
	scom.Close();
	acabar=1;
	pthread_join(thid_clientes, 0);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	if(nombres.size()!=num_espectadores){
		printf("\nJUGADOR\n\n");
		printf(buffersc);
		printf("\n\nEspectadores\n\n");
		if(nombres.size()>1){
			for(int i=1;i<nombres.size();i++){	
			strcpy(espectadores[i],nombre);
			printf(espectadores[i]);
			printf(",");
			}
	
			printf(espectadores[0]);
		}
		else if(nombres.size()!=0){
			strcpy(espectadores[0],nombre);
			printf(espectadores[0]);
		}
		printf("\n");
	}
	else if((nombres.size()==0)&&(mostrar_jugador==1)){
		printf("\nJUGADOR\n\n");
		printf(buffersc);	
		printf("\n");
		mostrar_jugador=0;
	}
	num_espectadores=nombres.size();	
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	char cad_s[50];


	m->raqueta1=jugador1;
	m->esfera=esfera;

	if(m->accion==1)OnKeyboardDown('w',0,0);
	else if(m->accion==-1)OnKeyboardDown('s',0,0);


	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		char cad[100];
		sprintf(cad," jugador 2 marco 1 punto,lleva %d \n",puntos2);
		write (tuberia,cad,strlen(cad)+1);

		if (esfera.radio>0.1f)
			{
				esfera.radio=esfera.radio-0.05f;
			}
		else esfera.radio=0.1f;
		
		if(puntos2==3)
		{
		printf("\n El jugador 2 ha ganado %d-%d al jugador 1\n",puntos2,puntos1);
		close(tuberia);
		exit(1);
		}
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		char cad[100];
		sprintf(cad," jugador 1 marco 1 punto,lleva %d \n",puntos1);
		write (tuberia,cad,strlen(cad)+1);

		if (esfera.radio>0.1f)
			{
				esfera.radio=esfera.radio-0.05f;
			}
		else esfera.radio=0.1f;

		if(puntos1==3)
		{
		printf("\n El jugador 1 ha ganado %d-%d al jugador 2\n",puntos1,puntos2);
		close(tuberia);
		exit(1);
		}

	}
 
//Contruccion de la cadena a enviar por la fifo servidor-cliente y write de la cadena

	sprintf(buffersc,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	//write(fifosc,&buffersc,sizeof(buffersc));

	//Envio de las coordenadas mediante socket
	socket=scom.Send(buffersc,sizeof(buffersc));
	if(socket<0){
		perror("Error al enviar");
		exit(0);
	}


//for (int i=conexiones.size()-1;i>=0;i--)
//	conexiones[i].Send(buffersc,200);
//Codigo desconexion de clientes
	for(int i=conexiones.size()-1;i>=0;i--)
		if(conexiones[i].Send(buffersc,200)<=0){
			conexiones.erase(conexiones.begin()+1);
			if(i<2){
			puntos1=0;
			puntos2=0;			
			}		
		}	

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}
void * hilo_gestion_sockets(void *d){
	CMundo * p=(CMundo * )d;
	p->GestionarConexiones();
}

void CMundo::GestionarConexiones()
{
	while (!acabar) {
		char nombre[200];
		Socket auxiliar;
		auxiliar=scon.Accept();
		conexiones.push_back(auxiliar);
		auxiliar.Receive(nombre,sizeof(nombre));
		nombres.push_back(nombre);
		strcpy(nombre,nombres[nombres.size()-1]);
	}
}

void CMundo::Init()
{	
	char cads[50];
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;

	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
//logger
	tuberia=open("/tmp/TuberiaLogger",O_WRONLY);

//fifo servidor-cliente apertura de la tuberia

	//fifosc=open("/tmp/Tuberiasc",O_WRONLY);
//fifo cliente-servido teclas

	//fifocs=open("/tmp/Tuberiacs", O_RDONLY); 

//Trhrad cliente servidor
	//pthread_create(&thrd, NULL, hilo_comandos, this);

//Esto solo es para el Cliente
//bot
	//Abro fichero
	mem=open("/tmp/mem",O_CREAT|O_RDWR,0666);//creo el archivo y puedo leer y escribir.
	
	//Escribo
	write(mem,&datos,sizeof(datos));
	
	//Proyecto en memoria	
	m=(DatosMemCompartida*) mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,mem,0);		

	//Cierro fichero
	close(mem);

	//Practica5
	
	
	pthread_create(&thrd, NULL, hilo_comandos, this);
	pthread_create(&thid_clientes, NULL,hilo_gestion_sockets,this);
//Socket
	
	sprintf(ip,"127.0.0.1");
	socket=scon.InitServer((char *)ip,2000);
	if (socket<0){
		perror("Error al crear el socket");
		
	}
	else{
		printf("Conexion establecida\n");
		scom=scon.Accept();
	}
	
	//Recibimos el nombre del cliente
	socket=scom.Receive(cads,sizeof(cads));
	if(socket<0) {
		perror("Error al recibir nombre del cliente\n");
		
			
	}
	else{
		printf("El nombre del cliente es: %s\n",cads);
	}
	
	acabar=0;
	num_espectadores=0;
	mostrar_jugador=1;
	
}

void CMundo::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];
            //read(fifocs, cad, sizeof(cad));
            //Con sockets
	    socket=scom.Receive(cad,sizeof(cad));
		if(socket==0) break;
		//printf("%d",socket);
	    unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}

